def add(a,b)
  a + b
end

def subtract(a,b)
  a - b
end

def sum(arr)
  arr.reduce(0,:+)
end
