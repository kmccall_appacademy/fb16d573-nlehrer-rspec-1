def translate(sentence)
  sentence.split(' ').map{ |word| transword(word) }.join(' ')
end

def transword(word)
  first_vowel = word.index(/[aeiou]/)
  if word[first_vowel-1..first_vowel] == 'qu'
    first_vowel = first_vowel + 1
  end
  if first_vowel == 0
    "#{word}ay"
  else
    "#{word[first_vowel..-1]}#{word[0..first_vowel-1]}ay"
  end
end
