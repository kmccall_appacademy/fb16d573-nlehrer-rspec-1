def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, times = 2)
  ([str] * times).join(' ')
end

def start_of_word(str, letters)
  str[0..letters - 1]
end

def first_word(str)
  str.split(' ')[0]
end

def titleize(str)
  arr_old = str.split(' ')
  arr_new = [arr_old[0].capitalize] + arr_old[1..-1].map { |s| titlecase(s) }
  arr_new.join(' ')
end

def titlecase(str)
  if %w[and the over].include?(str)
    str
  else
    str.capitalize
  end
end
